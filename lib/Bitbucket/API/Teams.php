<?php

/**
 * This file is part of the bitbucket-api package.
 *
 * (c) Alexandru G. <alex@gentle.ro>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Bitbucket\API;

use Buzz\Message\MessageInterface;

/**
 * @author  Alexandru G.    <alex@gentle.ro>
 */
class Teams extends Api
{
    /**
     * Get a list of teams to which the caller has access.
     *
     * @access public
     * @param  string           $role Will only return teams on which the user has the specified role.
     * @return MessageInterface
     *
     * @throws \InvalidArgumentException
     */
    public function all($role)
    {
        if (!is_string($role)) {
            throw new \InvalidArgumentException(sprintf('Expected $role of type string and got %s', gettype($role)));
        }

        if (!in_array(strtolower($role), array('member', 'contributor', 'admin'), true)) {
            throw new \InvalidArgumentException(sprintf('Unknown role %s', $role));
        }

        return $this->getClient()->setApiVersion('2.0')->get('teams', array('role' => $role));
    }

    /**
     * Get the public information associated with a team.
     *
     * @access public
     * @param  string           $name The team's name.
     * @return MessageInterface
     */
    public function profile($name)
    {
        return $this->getClient()->setApiVersion('2.0')->get(
            sprintf('teams/%s', $name)
        );
    }

    /**
     * Get the team members.
     *
     * @access public
     * @param  string           $name The team's name.
     * @return MessageInterface
     */
    public function members($name)
    {
        return $this->getClient()->setApiVersion('2.0')->get(
            sprintf('teams/%s/members', $name)
        );
    }

    /**
     * Get the team followers list.
     *
     * @access public
     * @param  string           $name The team's name.
     * @return MessageInterface
     */
    public function followers($name)
    {
        return $this->getClient()->setApiVersion('2.0')->get(
            sprintf('teams/%s/followers', $name)
        );
    }

    /**
     * Get a list of accounts the team is following.
     *
     * @access public
     * @param  string           $name The team's name.
     * @return MessageInterface
     */
    public function following($name)
    {
        return $this->getClient()->setApiVersion('2.0')->get(
            sprintf('teams/%s/following', $name)
        );
    }

    /**
     * Get the team's repositories.
     *
     * @access public
     * @param  string           $name The team's name.
     * @return MessageInterface
     */
    public function repositories($name)
    {
        return $this->getClient()->setApiVersion('2.0')->get(
            sprintf('teams/%s/repositories', $name)
        );
    }

    /**
    * Create a project
    * @param string $owner The username or uuid
    * @param string $name The project name
    * @param array $params Parameters for the request
    * @return MessageInterface
    */
    public function createProject($owner, $name, $params = array())
    {

        $defaults = array(
            'name'              => $name,
            'key'               => strtoupper(substr($name, 0, 4)),
            'description'       => '',
            'is_private'        => true,
        );

        // allow developer to directly specify params as json if (s)he wants.
        if ('array' !== gettype($params)) {
            if (empty($params)) {
                throw new \InvalidArgumentException('Invalid JSON provided.');
            }

            $params = $this->decodeJSON($params);
        }

        $params = json_encode(array_merge($defaults, $params));

        return $this->getClient()->setApiVersion('2.0')->post(
            sprintf('teams/%s/projects/', $owner),
            $params,
            array('Content-Type' => 'application/json')
        );
    }
}
